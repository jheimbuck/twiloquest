import sys

# This code reads in arguments and converts those inputs to decimal numbers
num1 = int(sys.argv[1])
num2 = int(sys.argv[2])

sum_to_use = num1 + num2

#print(f"num1 is {num1}")
#print(f"num2 is {num2}")
#print(f"sum is {sum_to_use}")

def sum_check(sum_to_use):
  if sum_to_use <= 0:
    print(f"You have chosen the path of destitution.")
  elif sum_to_use <= 100:
    print(f"You have chosen the path of plenty.")
  else:
    print(f"You have chosen the path of excess.")

sum_check(sum_to_use)
